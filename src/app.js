const express = require( 'express' );
const app = express();
const _PORT = 3000;
app.set( 'view engine', 'ejs' );
app.use( '/static', express.static('public') );
app.get( '/', ( req, res ) => {
    res.render( 'index' );
} );
app.listen( _PORT, _ => {
    console.log( `server running at port ${_PORT}` )
} );